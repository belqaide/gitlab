const mongoose=require('mongoose');
const ProjetSchema=mongoose.Schema({
   
    name :{
        type:String
    },
    age:{
        type:Number,
        min:18
    },
    subDate:{
        type:Date,
        default:Date.now
    }
    
});
module.exports=mongoose.model('Projet',ProjetSchema);
