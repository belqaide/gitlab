const Projet=require('../Models/Projet.model')

//find
exports.findAll = (req,res)=>{
    Projet.find().then(
        data=>{
        res.send(data);
    })
    .catch(err=>{
        res.status(500).send({    //renvoyer une réponse HTTP avec un code d'état de 500
            message:err.message||"some error"
        });
    });
};
//find by id
exports.findbyid = (req,res)=>{
    const id=req.params.id    //récupérer la valeur d'un paramètre nommé "id" dans une requête HTTP.
    Projet.findById( req.params.id).then(
        data=>{
        res.send(data);
    })
    .catch(err=>{
        res.status(500).send({
            message:err.message||"some error"
        });
    });
};

//delete
 exports.deletebyid =(req,res)=>{
    console.log('ok')
    Projet.deleteOne({_id:req.params.id}).then(
        data=>{
            res.send(data);
        })
        .catch(err=>{
            res.status(500).send({
                message:err.message||"error"
            })
        })
 }

 //update
 exports.updatebyname =(req,res)=>{
    console.log('ok')
    Projet.updateOne({name:req.params.name},{$set:newname}).then(
        data=>{
            res.send(data);
        })
        .catch(err=>{
            res.status(500).send({
                message:err.message||"error"
            })
        })
 }
//update by id
exports.updateById = (req,res)=>{
    
    Projet.updateById({_id:req.params.id}).then(
        data=>{
        res.send(data);
    })
    .catch(err=>{
        res.status(500).send({
            message:err.message||"some error"
        });
    });
};
 //create
exports.create =async(req,res)=>{
    const user=new Projet({
        name:req.body.name,
        age:req.body.age,
    })
    await user.save().then(data=>{
        res.send(data)
    })
    .catch(err=>{
        res.status(500).send({
            message:err.message||"error"
        })
    })
}


exports.update = (req, res) => {
    const id = req.params.id;
  
    Projet.findByIdAndUpdate(id, req.body, { new: true,useFindAndModify: false })
      .then(data => {
        if (!data) {
          res.status(404).send({
            message: `Cannot update Student with id=${id}.  not found`
          });
        } else res.send({ message: " successfully.",data:data });
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating  with id=" + id
        });
      })
  };