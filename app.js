const express =require('express');
const mongoose = require('mongoose');
const app=express();
//activer le middleware qui analyse les requêtes entrantes avec des charges utiles JSON.
app.use(express.json());
const rte=require('./Routes/Myprj.routes');   //importer un module
app.use('/',rte)
app.listen(3030);

const mdl =require('./Models/Projet.model');
app.use('/',mdl);
//mongoose.connect(process.env.MONGO_CONNECT,{},()=>console.log('connected'));

//permet de définir la valeur de la propriété "strictQuery" de Mongoose sur "true".
 mongoose.set('strictQuery',true);

 // se connecter à une base de données MongoDB.
 mongoose.connect('mongodb://127.0.0.1:27017/db-cl',{
   useNewUrlParser:true,
   useUnifiedTopology:true,
   socketTimeoutMS:43200000,
}).then(
   ()=>{console.log('connected to digitalOceon mongodb: db-cl');},
   err=>{console.log('err to connect digitalOceon mongodb:'+err);}
);

//ajout
 
async function add(){
   try{
      const mdhl=await mdl.create(     // créer un nouveau document dans une collection de base de données MongoDB à l'aide d'un model défini par Mongoose
         {
            name :'adam',
            age:22
         }
      );
         await mdhl.save();
         console.log('added',mdhl);
   }catch(err){
      console.log(err.message)
   }
}

 

//find
 
async function findbyid(){
   const mdls=await mdl.findById('63f0b2b2c88b9a723d24eff6')
   console.log('found',mdls)
}

//delete
 
async function deletebyid(){
   const mds=await mdl.deleteOne({_id:'63f8fbe7d4d5e96f31cb6765'})
   console.log('delete',mds)
}

//update
 
async function updatebyname(){
   const mds=await mdl.findById('63f0b298da14c08b0e6ede7a')
   console.log('before')
   mds.name="alae"
  mds.save()
   console.log('update',mds)
}
